Mando Makes Dotfiles
---

This repo contains all the dotfiles for my personal Linux setup. It will be added to and tweaked as time goes on.

![Image](/screenshots/neofetch.png)
